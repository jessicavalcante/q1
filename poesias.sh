#!/bin/bash

# Imprime uma frase em uma cor aleatória
print_colored_phrase() {
    # Lista de códigos de cores ANSI
    colors=("31" "32" "33" "34" "35" "36")
    # Seleciona uma cor aleatória da lista
    color_index=$(($RANDOM % 6))
    color_code=${colors[$color_index]}
    
    # Imprime a frase na cor selecionada
    echo -e "\e[${color_code}m$1\e[0m"
}

# Imprime cada frase com um intervalo aleatório entre 0.5 e 3 segundos
print_colored_phrase "Rosas são azuis,"
sleep $(($RANDOM % 3 + 1))
print_colored_phrase "Violetas são vermelhas,"
sleep $(($RANDOM % 3 + 1))
print_colored_phrase "O amor é doce,"
sleep $(($RANDOM % 3 + 1))
print_colored_phrase "E também é amargo,"
sleep $(($RANDOM % 3 + 1))
print_colored_phrase "Mas contigo, tudo é um sonho."
